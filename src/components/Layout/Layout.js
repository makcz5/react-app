import React from 'react';
import Header from '../Layout/Header'
import Content from '../Layout/Content'

const Layout = () => (
    <div>
        <Header /> 
        <Content />       
    </div>
);

export default Layout;