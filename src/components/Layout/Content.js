import React from 'react';
import { Route } from 'react-router-dom';

import MoviesList from '../../containers/movies-list';
import MovieDetailed from '../../containers/movie-detailed';


const Content = () => (
    <div id="content">
        <Route path = '/' exact component = { MoviesList } />
        <Route path = '/:id' component = { MovieDetailed } />
    </div> 
);

export default Content;