import React from 'react';

const Movie = (props) => (
    <li className="movie-item">
        <div className="thumb">            
            <img className="poster" src={props.poster} alt="" />
        </div>
        <div className="descr">
            <h2 className="title">{props.title}</h2>
            <span className="year">{props.year}</span>
            <span className="type">{props.type}</span>
        </div>  
    </li>
);

export default Movie;