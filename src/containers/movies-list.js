import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import axios from 'axios';
import Movie from '../containers/movie';

const MoviesList = React.memo(props => {

    const [movies, updateMovies] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");

    const handleChange = event => {
        const url = 'http://www.omdbapi.com/?apikey=f79aeba3&s=' + event.target.value;
        setSearchTerm(event.target.value);

        if (event.target.value.length > 2) {
            axios.get(url)
                .then(response => {
                    updateMovies(response.data.Search);
                });
        }
    };

    return (
        <div className="movies">
            <form>
                <h1>Search Your Favorite Movie</h1>
                <p><input type="text" placeholder="enter title" value={searchTerm} onChange={handleChange} /></p>

                <button className="button" type="submit" onChange={handleChange}>Search</button>
            </form>

            <ul className="movies-list">
                {movies ?
                    movies.map(movie => {
                        return (
                            <Link to={'/' + movie.imdbID} key={movie.imdbID}>
                                <Movie
                                    title={movie.Title}
                                    year={movie.Year}
                                    type={movie.Type}
                                    poster={movie.Poster}                             
                                />
                            </Link>
                        )
                    }) : false}
            </ul>
        </div>
    )
});

export default MoviesList;