import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const MovieDetailed = React.memo(props => {
    const [movie, updateMovie] = useState([]);
    const url = 'http://www.omdbapi.com/?apikey=f79aeba3&i=' + props.match.params.id;

    useEffect(() => {
        axios.get(url)
        .then(response => {
            updateMovie(response.data);
        });
      },[]);

    return (
        <div className="movieDetailed">
            {
            <div>
                <div className="thumb">
                    <img className="poster" src={movie.Poster} alt="" />
                </div>
                <div className="descr">
                    <h1 className="title">{movie.Title}</h1>
                    <div className="details">
                        <span className="year">{movie.Title}</span>
                        <span className="type">{movie.Type}</span>
                        <span className="plot">{movie.Plot}</span>
                        <span className="rating">{movie.Rating}</span>
                        <span className="runtime">{movie.Runtime}</span>
                        <span className="genre">{movie.Genre}</span>
                    </div>
                    <Link to={'/'} className="button">Back To Search Results</Link>
                </div>
            </div>
            }
        </div>
    )
});

export default MovieDetailed;